package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;	
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Daniel S Sapato
	 */
	public class Game 
	{
		/******Variables******/
		
		//Textures
		[Embed(source="Resources/Terrain/grass-texture-2.jpg")]
		private var grass:Class;
		private var grassSprite:Sprite;				
		
		//Input
		public static var mousePos:Point;
		public static var mouse_down:Boolean;
		public static var mouse_click:Boolean;
		
		private var keys_down:Vector.<uint>;
		private const LEFT:int = 37;
		private const UP:int = 38;
		private const RIGHT:int = 39;
		private const DOWN:int = 40;
		private const W:int = 87;
		private const A:int = 65;
		private const S:int = 83;
		private const D:int = 68;
		private const Q:int = 81;
		private const E:int = 69;
		private const SPACE:int = 32;
		private const ESC:int = 27;
		private const ONE:int = 49;
		private const TWO:int = 50;
		private const THREE:int = 51;
		private const FOUR:int = 52;
		private const FIVE:int = 53;
		private const SIX:int = 54;
		private const SEVEN:int = 55;
		private const EIGHT:int = 56;
		
		//Rendering
		public var bitmap:Bitmap;
		public static var Renderer:BitmapData;	
		
		//Player
		public var player:Player;
		
		
		//Constructor
		public function Game(width: int, height: int) {
			//Textures
			grassSprite = new Sprite();
			grassSprite.addChild(new grass());
			
			//Input
			mousePos = new Point(0, 0);
			mouse_down = false;
			mouse_click = false;
			keys_down = new Vector.<uint>;
			
			//Visual
			Renderer = new BitmapData(width, height, false, 0x000000);
			bitmap = new Bitmap(Renderer);
			
			//Player
			player = new Player(300, 300, 32);
		}
		
		public function update():void {
			player.update();
		}
		
		public function render():void {
			Renderer.lock();
			
			//Draw background
			Renderer.draw(grassSprite);			
			
			//Player
			player.render();
			
			//Mouse
			
			Renderer.unlock();
		}
		
		public function mouseUp(e:MouseEvent):void {
			mouse_down = false;
		}
		
		public function mouseDown(e:MouseEvent):void {
			mouse_down = true;
			mouse_click = true;
		}
		
		public function mouseMove(e:MouseEvent):void {
			mousePos.x = e.stageX;
			mousePos.y = e.stageY;
		}
		
		public function mouseWheel(e:MouseEvent):void {
			
		}
		
		public function keyUp(e:KeyboardEvent):void {
			//check if key released is in the array
			for (var i:int = 0; i < keys_down.length; i++) {
				if (e.keyCode == keys_down[i]) {
					keys_down.splice(i, 1);
					break;
				}
			}
		}
		
		public function keyDown(e:KeyboardEvent):void {
			//Check if already in pressed array
			for (var i:int = 0; i < keys_down.length; i++ ) {
				if (keys_down[i] == e.keyCode) return;//Key already down
			}
			
			keys_down.push(e.keyCode);
		}
		
		public function checkKeyDown(keycode:int):Boolean {
			for (var i:int = 0; i < keys_down.length; i++ ) {
				if (keys_down[i] == keycode) {
					return true;
				}
			}
			return false;
		}		
	}

}