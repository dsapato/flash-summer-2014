package GameObject 
{
	/**
	 * ...
	 * @author Daniel S Sapato
	 */
	public class GameObject 
	{
		public var x:Number;
		public var y:Number;
		public var vx:Number;
		public var vy:Number;
		public var width:int;
		public var height:int;		
		
		private const friction:Number = .99;
		
		public function GameObject(xx:int, yy:int, w:int, h:int) {
			x = xx;
			y = yy;
			vx = 0;
			vy = 0;
			width = w;
			height = h;
		}
		
		public function update():void {
			//X Movement
			if (vx != 0) {
				x += vx;
				vx *= friction;
			}
			
			//Y Movement
			if (vy != 0) {
				y += vy;
				vy *= friction;
			}	
			
			//Check bounds
			if (x < 0) x = 0;
			if (x + width > Game.Renderer.width) x = Game.Renderer.width - width;
			if (y < 0) y = 0;
			if (y + height > Game.Renderer.height) y = Game.Renderer.height - height;	
		}
		
		public function render():void {
			
		}
		
	}

}