package  
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import GameObject.GameObject;
	/**
	 * ...
	 * @author Daniel S Sapato
	 */
	public class Player extends GameObject
	{
		private var sprite:Sprite;
		
		private const acceleration = .1;
		
		public function Player(x:Number, y:Number, size:int){
			super(x, y, size, size);
			
			//Sprite
			sprite = new Sprite();
			sprite.graphics.beginFill(0xCCFF00);
					
		}
		
		override public function update():void {
			super.update();
		}
		
		override public function render():void {
			sprite.graphics.drawRect(x, y, width, height);
			
		}
		
		public function moveUp():void {
			vy -= acceleration;
		}
		
		public function moveDown():void {
			vy += acceleration;
		}
		
		public function moveLeft():void {
			vx -= acceleration;
		}
		
		public function moveRight():void {
			vx += acceleration;
		}		
	}

}