package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;

	/**
	 * ...
	 * @author Daniel S Sapato
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		//Global Variables
		private var game:Game;		

		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			//create game object
			game = new Game(stage.stageWidth, stage.stageHeight);
			
			//add game bitmap
			addChild(game.bitmap);
			
			//game loop
			addEventListener(Event.ENTER_FRAME, run);
			
			//mouse
			stage.addEventListener(MouseEvent.MOUSE_UP, game.mouseUp);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, game.mouseDown);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, game.mouseMove);
			stage.addEventListener(MouseEvent.MOUSE_WHEEL, game.mouseWheel);
			
			//keys
			stage.addEventListener(KeyboardEvent.KEY_DOWN, game.keyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, game.keyUp);			
		}

		private function run(e:Event):void{
			game.update();
			game.render();
		}		
	}

}